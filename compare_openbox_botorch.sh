# for i in {1..5}
# do
#   python test/reproduction/mo/benchmark_mo_openbox_math.py --problem zdt2-3
#   python test/reproduction/mo/benchmark_mo_botorch_math.py --problem zdt2-3
# done

# for i in {1..5}
# do
#   python test/reproduction/mo/benchmark_mo_openbox_math.py --problem dtlz1-6-5
#   python test/reproduction/mo/benchmark_mo_botorch_math.py --problem dtlz1-6-5
# done

# python test/reproduction/moc/benchmark_moc_openbox_math.py --problem constr --rep 5 --n 200
# python test/reproduction/moc/benchmark_moc_botorch_math.py --problem constr --rep 5 --n 200
python test/reproduction/moc/benchmark_moc_hypermapper_math.py --problem constr --rep 5 --n 200
python test/reproduction/moc/benchmark_moc_optuna_math.py --problem constr --rep 5 --n 200

python test/reproduction/moc/benchmark_moc_openbox_math.py --problem srn --rep 5 --n 200
python test/reproduction/moc/benchmark_moc_botorch_math.py --problem srn --rep 5 --n 200
python test/reproduction/moc/benchmark_moc_hypermapper_math.py --problem srn --rep 5 --n 200
python test/reproduction/moc/benchmark_moc_optuna_math.py --problem srn --rep 5 --n 200