# 获取文件夹路径
folder_path="./test/data/cls_datasets"

# 获取文件夹下所有文件名并截取".csv"前面的名称形成列表A
A=($(ls "$folder_path"/*.csv | sed 's/.*\/\([^\/]*\)\.csv/\1/'))

# 遍历列表A，运行Python脚本
for name in "${A[@]}"; do
#    python test/reproduction/fioc/benchmark_fioc_openbox_math.py --problem svc_$name --rep 5
#    python test/reproduction/fioc/benchmark_fioc_smac_math.py --problem svc_$name --rep 5
#    python test/reproduction/fioc/benchmark_fioc_ax_math.py --problem svc_$name --rep 5
    python test/reproduction/fioc/benchmark_fioc_optuna_math.py --problem svc_$name --rep 5
#    python test/reproduction/fioc/benchmark_fioc_hypermapper_math.py --problem svc_$name --rep 5
done

#python test/reproduction/fioc/benchmark_fioc_openbox_math.py --problem nasbench201_cifar10 --rep 5
#python test/reproduction/fioc/benchmark_fioc_smac_math.py --problem nasbench201_cifar10 --rep 5
#python test/reproduction/fioc/benchmark_fioc_ax_math.py --problem nasbench201_cifar10 --rep 5

#python test/reproduction/fioc/benchmark_fioc_openbox_math.py --problem nasbench201_cifar100 --rep 5
#python test/reproduction/fioc/benchmark_fioc_smac_math.py --problem nasbench201_cifar100 --rep 5
#python test/reproduction/fioc/benchmark_fioc_ax_math.py --problem nasbench201_cifar100 --rep 5
python test/reproduction/fioc/benchmark_fioc_optuna_math.py --problem nasbench201_cifar100 --rep 5
python test/reproduction/fioc/benchmark_fioc_hypermapper_math.py --problem nasbench201_cifar100 --rep 5

# python test/reproduction/fioc/benchmark_fioc_openbox_math.py --problem nasbench201_ImageNet16-120 --rep 5
# python test/reproduction/fioc/benchmark_fioc_smac_math.py --problem nasbench201_ImageNet16-120 --rep 5
# python test/reproduction/fioc/benchmark_fioc_ax_math.py --problem nasbench201_ImageNet16-120 --rep 5
