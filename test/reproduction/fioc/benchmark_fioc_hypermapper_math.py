

"""
example cmdline:

python test/optimizer/so/benchmark_so_openbox_math.py --problem branin --n 200 --init 3 --surrogate gp --optimizer scipy --rep 1 --start_id 0

"""
import os
import sys
import time
import pandas as pd
import numpy as np
import argparse
import pickle as pkl
import json
from hypermapper import optimizer
import warnings
warnings.filterwarnings("ignore")

sys.path.insert(0, os.getcwd())
from test.reproduction.fioc.fioc_benchmark_function import get_problem
from test.reproduction.test_utils import timeit, seeds

from ConfigSpace import ConfigurationSpace, UniformFloatHyperparameter, UniformIntegerHyperparameter, \
    Constant, CategoricalHyperparameter, InCondition, EqualsCondition, UnParametrizedHyperparameter, \
    ForbiddenEqualsClause, ForbiddenInClause, ForbiddenAndConjunction


parser = argparse.ArgumentParser()
parser.add_argument('--problem', type=str)
parser.add_argument('--n', type=int, default=100)
parser.add_argument('--nRS', type=int, default=10)
parser.add_argument('--rep', type=int, default=1)
parser.add_argument('--start_id', type=int, default=0)

args = parser.parse_args()
problem_str = args.problem
number_of_RS = args.nRS
max_runs = args.n

rep = args.rep
start_id = args.start_id
mth = 'hypermapper'

problem = get_problem(problem_str)
max_runtime_per_trial = 600
task_id = '%s_%s' % (mth, problem_str)


def evaluate(mth, run_i, seed):
    print(mth, run_i, seed, '===== start =====', flush=True)

    def objective_function(config):
        y = problem.evaluate_config(config, optimizer=mth)
        res = {}
        res['f1_value'] = y['objectives']
        res['Valid'] = np.all(y['constraints'] <= 0)

        return res

    objs = ["f%d_value" % (k + 1) for k in range(problem.num_objectives)]
    scenario = {}
    scenario["application_name"] = problem_str
    scenario["optimization_objectives"] = objs

    scenario["design_of_experiment"] = {}
    scenario["design_of_experiment"]["number_of_samples"] = number_of_RS
    scenario['optimization_method'] = 'bayesian_optimization'
    scenario["optimization_iterations"] = max_runs

    scenario["models"] = {}
    scenario["models"]["model"] = "gaussian_process"

    scenario["feasible_output"] = {}
    scenario["feasible_output"]["enable_feasible_predictor"] = True

    input_parameters = problem.get_configspace(optimizer='smac')
    scenario["input_parameters"] = {}
    for i, v in enumerate(input_parameters.values()):
        name = v.name
        if isinstance(v, UniformFloatHyperparameter):
            scenario["input_parameters"][name] = {"parameter_type": "real", "values": [v.lower, v.upper]}
        elif isinstance(v, UniformIntegerHyperparameter):
            scenario["input_parameters"][name] = {"parameter_type": "integer", "values": [v.lower, v.upper]}
        elif isinstance(v, CategoricalHyperparameter):
            scenario["input_parameters"][name] = {"parameter_type": "categorical", "values": v.choices}
        elif isinstance(v, Constant):
            scenario["input_parameters"][name] = {"parameter_type": "categorical", "values": [v.value]}

    with open("hypermapper_scenario.json", "w") as scenario_file:
        json.dump(scenario, scenario_file, indent=4)

    optimizer.optimize("hypermapper_scenario.json", objective_function)

    # bo.run()

    # Read the CSV file
    df = pd.read_csv('%s_output_samples.csv' % problem_str)
    # If 'Valid' is False, set 'f1_value' to 9999999.0

    # Extract the columns starting with 'x' into a 2D array
    x_cols = df.columns[df.columns.str.startswith('x')]
    config_list = df[x_cols].values

    # Extract the 'f1_value' column into a variable
    perf_list = df[objs].values

    # Extract the 'Timestamp' column and subtract the previous value from each entry
    time_list = df['Timestamp'].values

    return config_list, perf_list, time_list


with timeit('%s all' % (mth,)):
    for run_i in range(start_id, start_id + rep):
        seed = seeds[run_i]
        with timeit('%s %d %d' % (mth, run_i, seed)):
            # Evaluate
            config_list, perf_list, time_list = evaluate(mth, run_i, seed)

            # Save result
            print('=' * 20)
            print(seed, mth, config_list, perf_list, time_list)
            print(seed, mth, 'best perf', np.min(perf_list))

            timestamp = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))
            dir_path = 'logs/fioc_benchmark_%s_%d/%s/' % (problem_str, max_runs, mth)
            file = 'benchmark_%s_%04d_%s.pkl' % (mth, seed, timestamp)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
            with open(os.path.join(dir_path, file), 'wb') as f:
                save_item = (config_list, perf_list, time_list)
                pkl.dump(save_item, f)
            print(dir_path, file, 'saved!', flush=True)


