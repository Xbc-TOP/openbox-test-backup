import os
import pandas as pd
import numpy as np
from ConfigSpace import ConfigurationSpace, UniformFloatHyperparameter, UniformIntegerHyperparameter, \
    Constant, CategoricalHyperparameter, InCondition, EqualsCondition, UnParametrizedHyperparameter, \
    ForbiddenEqualsClause, ForbiddenInClause, ForbiddenAndConjunction


def get_problem(problem_str, **kwargs):
    # problem_str = problem_str.lower()  # dataset name may be uppercase
    if 'svc' in problem_str:
        problem = svc
        kwargs['dataset'] = '_'.join(problem_str.split('_')[1:])
    elif 'nasbench201' in problem_str:
        problem = nasbench201
        kwargs['dataset'] = problem_str.split('_')[1]
    else:
        raise ValueError('Unknown problem_str %s.' % problem_str)
    return problem(**kwargs)


def load_data(dataset, data_dir):
    # load csv data
    df = pd.read_csv(os.path.join(data_dir, dataset + '.csv'))

    # 提取特征变量X和目标变量y
    X = df.iloc[:, :-1].values  # 选择除了最后一列之外的所有列作为特征变量
    y = df.iloc[:, -1].values  # 选择最后一列作为目标变量

    return X, y


class BaseFIOCProblem:
    def __init__(self, dim, num_objectives, **kwargs):
        self.dim = dim
        self.num_objectives = num_objectives

    def evaluate_config(self, config, optimizer='smac'):
        raise NotImplementedError

    def evaluate(self, X: np.ndarray):
        raise NotImplementedError

    @staticmethod
    def get_config_dict(config, optimizer='smac'):
        if optimizer == 'smac':
            config_dict = config.get_dictionary().copy()
        elif optimizer in ['tpe', 'ax', 'optuna', 'optuna_nsgaii', 'hypermapper']:
            config_dict = config
        else:
            raise ValueError('Unknown optimizer %s' % optimizer)
        return config_dict

    @staticmethod
    def checkX(X: np.ndarray):
        X = np.atleast_2d(X)
        assert len(X.shape) == 2 and X.shape[0] == 1
        X = X.flatten()
        return X

    def get_configspace(self, optimizer='smac'):
        raise NotImplementedError

    def load_data(self, **kwargs):
        from test.reproduction.test_utils import load_data
        from sklearn.model_selection import train_test_split
        dataset = kwargs['dataset']
        data_dir = kwargs.get('data_dir', './data/cls_datasets/')
        data_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../../', data_dir)

        x, y = load_data(dataset, data_dir)

        self.train_x, self.val_x, self.train_y, self.val_y = \
            train_test_split(x, y, stratify=y, random_state=1, test_size=0.3)


class svc(BaseFIOCProblem):
    def __init__(self, **kwargs):
        super().__init__(dim=8, num_objectives=1, **kwargs)
        self.load_data(**kwargs)
        self.bounds = None

    def evaluate_config(self, config, optimizer='smac'):
        config_dict = self.get_config_dict(config, optimizer)
        penalty = config_dict['penalty']
        loss = config_dict.get('loss', None)
        dual = config_dict.get('dual', None)
        C = config_dict['C']
        tol = config_dict['tol']
        fit_intercept = bool(config_dict['fit_intercept'])
        intercept_scaling = config_dict['intercept_scaling']
        if isinstance(penalty, dict):
            combination = penalty
            penalty = combination['penalty']
            loss = combination['loss']
            dual = combination['dual']

        from sklearn.svm import LinearSVC
        from sklearn.metrics._scorer import balanced_accuracy_scorer
        if dual == 'True':
            dual = True
        elif dual == 'False':
            dual = False

        y = 0
        valid = -1
        try:
            svcc = LinearSVC(penalty=penalty,
                             loss=loss,
                             dual=dual,
                             tol=tol,
                             C=C,
                             fit_intercept=fit_intercept,
                             intercept_scaling=intercept_scaling,
                             multi_class='ovr',
                             random_state=1)
            svcc.fit(self.train_x, self.train_y)
            y = -balanced_accuracy_scorer(svcc, self.val_x, self.val_y)
        except:
            y = 0
            valid = 1

        return {'objectives': y, 'constraints': np.array([valid])}

    def get_configspace(self, optimizer='smac'):
        if optimizer == 'smac':
            cs = ConfigurationSpace()

            penalty = CategoricalHyperparameter(
                "penalty", ["l1", "l2"], default_value="l2")
            loss = CategoricalHyperparameter(
                "loss", ["hinge", "squared_hinge"], default_value="squared_hinge")
            dual = CategoricalHyperparameter("dual", ['True', 'False'], default_value='True')
            # This is set ad-hoc
            tol = UniformFloatHyperparameter(
                "tol", 1e-5, 1e-1, default_value=1e-4, log=True)
            C = UniformFloatHyperparameter(
                "C", 0.03125, 32768, log=True, default_value=1.0)
            multi_class = Constant("multi_class", "ovr")
            # These are set ad-hoc
            fit_intercept = Constant("fit_intercept", "True")
            intercept_scaling = Constant("intercept_scaling", 1)
            cs.add_hyperparameters([penalty, loss, dual, tol, C, multi_class,
                                    fit_intercept, intercept_scaling])

            penalty_and_loss = ForbiddenAndConjunction(
                ForbiddenEqualsClause(penalty, "l1"),
                ForbiddenEqualsClause(loss, "hinge")
            )
            constant_penalty_and_loss = ForbiddenAndConjunction(
                ForbiddenEqualsClause(dual, "False"),
                ForbiddenEqualsClause(penalty, "l2"),
                ForbiddenEqualsClause(loss, "hinge")
            )
            penalty_and_dual = ForbiddenAndConjunction(
                ForbiddenEqualsClause(dual, "True"),
                ForbiddenEqualsClause(penalty, "l1")
            )
            cs.add_forbidden_clause(penalty_and_loss)
            cs.add_forbidden_clause(constant_penalty_and_loss)
            cs.add_forbidden_clause(penalty_and_dual)
            return cs
        elif optimizer == 'tpe':
            from hyperopt import hp
            space = {'penalty': hp.choice('liblinear_combination',
                                          [{'penalty': "l1", 'loss': "squared_hinge", 'dual': "False"},
                                           {'penalty': "l2", 'loss': "hinge", 'dual': "True"},
                                           {'penalty': "l2", 'loss': "squared_hinge", 'dual': "True"},
                                           {'penalty': "l2", 'loss': "squared_hinge", 'dual': "False"}]),
                     'loss': None,
                     'dual': None,
                     'tol': hp.loguniform('liblinear_tol', np.log(1e-5), np.log(1e-1)),
                     'C': hp.loguniform('liblinear_C', np.log(0.03125), np.log(32768)),
                     'multi_class': hp.choice('liblinear_multi_class', ["ovr"]),
                     'fit_intercept': hp.choice('liblinear_fit_intercept', ["True"]),
                     'intercept_scaling': hp.choice('liblinear_intercept_scaling', [1])}
            return space
        elif optimizer == 'ax':
            parameters = [
                {
                    "name": "tol",
                    "value_type": "float",
                    "type": "range",
                    "bounds": [1e-5, 1e-1],
                    "log_scale": True
                },
                {
                    "name": "C",
                    "value_type": "float",
                    "type": "range",
                    "bounds": [0.03125, 32768],
                    "log_scale": True
                },
                {
                    "name": "multi_class",
                    "value_type": "str",
                    "type": "fixed",
                    "value": "ovr"
                },
                {
                    "name": "fit_intercept",
                    "value_type": "str",
                    "type": "fixed",
                    "value": "True"
                },
                {
                    "name": "intercept_scaling",
                    "value_type": "int",
                    "type": "fixed",
                    "value": 1
                },
                {
                    "name": "penalty",
                    "value_type": "str",
                    "type": "choice",
                    "values": ["l1", "l2"]
                },
                {
                    "name": "loss",
                    "value_type": "str",
                    "type": "choice",
                    "values": ["hinge", "squared_hinge"]
                },
                {
                    "name": "dual",
                    "value_type": "str",
                    "type": "choice",
                    "values": ['True', 'False']
                }
            ]

            return parameters
        else:
            raise ValueError('Unknown optimizer %s when getting configspace' % optimizer)



benchmark201_choices = [
    'none',
    'skip_connect',
    'nor_conv_1x1',
    'nor_conv_3x3',
    'avg_pool_3x3'
]


class nasbench201(BaseFIOCProblem):
    def __init__(self, **kwargs):
        super().__init__(dim=8, num_objectives=1, **kwargs)

        from nas_201_api import NASBench201API as API

        self.api = API(
            '/Users/xubeideng/Documents/Scientific Research/AutoML/automl_data/NAS-Bench-201-v1_1-096897.pth',
            verbose=False)

        self.dataset = kwargs['dataset']
        assert self.dataset in ['cifar10-valid', 'cifar10', 'cifar100', 'ImageNet16-120']

    def evaluate_config(self, config, iepoch=200, optimizer='smac'):

        config_dict = self.get_config_dict(config, optimizer)

        # convert config to arch
        arch = '|%s~0|+|%s~0|%s~1|+|%s~0|%s~1|%s~2|' % (config_dict['op_0'], config_dict['op_1'], config_dict['op_2'],
                                                        config_dict['op_3'], config_dict['op_4'], config_dict['op_5'])

        # query
        info = self.api.get_more_info(arch, self.dataset, iepoch=iepoch - 1, hp='200', is_random=False)
        train_time = info['train-all-time']
        if self.dataset == 'cifar10-valid':
            val_perf = info['valid-accuracy']
            test_perf = info.get('test-accuracy', None)
        elif self.dataset == 'cifar10':
            val_perf = info['test-accuracy']
            test_perf = None
        elif self.dataset == 'cifar100':
            val_perf = info['valtest-accuracy']
            test_perf = info.get('test-accuracy', None)
        elif self.dataset == 'ImageNet16-120':
            val_perf = info['valtest-accuracy']
            test_perf = info.get('test-accuracy', None)
        else:
            raise ValueError

        return {'objectives': -val_perf / 100, 'constraints': np.array([-1])}

    def get_configspace(self, optimizer='smac'):
        if optimizer == 'smac':
            cs = ConfigurationSpace()
            for i in range(6):
                cs.add_hyperparameter(
                    CategoricalHyperparameter('op_%d' % i, choices=benchmark201_choices,
                                              default_value=benchmark201_choices[1]))

            return cs

        elif optimizer == 'tpe':
            from hyperopt import hp
            space = {}
            for i in range(6):
                space['op_%d' % i] = hp.choice('op_%d' % i, benchmark201_choices)
            return space

        elif optimizer == 'ax':
            parameters = []
            for i in range(6):
                parameters.append(
                    {
                        "name": "op_%d" % i,
                        "value_type": "str",
                        "type": "choice",
                        "values": benchmark201_choices
                    }
                )
            return parameters
