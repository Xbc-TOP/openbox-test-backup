from ax import optimize
from ax.service import OptimizationLoop
from ax.core.formatting_utils import data_and_evaluations_from_raw_data
"""
example cmdline:

python test/reproduction/so/benchmark_so_openbox_math.py --problem branin --n 200 --init 3 --surrogate gp --optimizer scipy --rep 1 --start_id 0

"""
import os
import sys
import time
import numpy as np
import argparse
import pickle as pkl

sys.path.insert(0, os.getcwd())
from test.reproduction.fioc.fioc_benchmark_function import get_problem
from openbox.optimizer.generic_smbo import SMBO
from test.reproduction.test_utils import timeit, seeds

parser = argparse.ArgumentParser()
parser.add_argument('--problem', default='svc_cpu_act', type=str)  # svc_bank32nh
parser.add_argument('--n', type=int, default=100)
parser.add_argument('--rep', type=int, default=1)
parser.add_argument('--start_id', type=int, default=0)

args = parser.parse_args()
problem_str = args.problem
max_runs = args.n

rep = args.rep
start_id = args.start_id
mth = 'ax'

problem = get_problem(problem_str)
cs = problem.get_configspace(optimizer='ax')
task_id = '%s_%s' % (mth, problem_str)


def evaluate(mth, run_i, seed):
    print(mth, run_i, seed, '===== start =====', flush=True)

    def objective_function(config):
        res = problem.evaluate_config(config, optimizer='ax')

        return res['objectives']

    loop = OptimizationLoop.with_evaluation_function(
        parameters=cs,
        evaluation_function=objective_function,
        minimize=True,
        total_trials=max_runs,
        random_seed=seed,
    )

    # bo.run()
    time_list = []
    global_start_time = time.time()
    for i in range(max_runs):
        loop.run_trial()

        global_time = time.time() - global_start_time
        time_list.append(global_time)

    experiment = loop.experiment
    config_list = [experiment.trials[i].arm.parameters for i in range(max_runs)]
    perf_list = np.array([[x] for x in experiment.lookup_data().df['mean']])
    time_list = np.array(time_list)

    return config_list, perf_list, time_list


if __name__ == '__main__':
    with timeit('%s all' % (mth,)):
        for run_i in range(start_id, start_id + rep):
            seed = seeds[run_i]
            with timeit('%s %d %d' % (mth, run_i, seed)):
                # Evaluate
                config_list, perf_list, time_list = evaluate(mth, run_i, seed)

                # Save result
                print('=' * 20)
                print(seed, mth, config_list, perf_list, time_list)
                print(seed, mth, 'best perf', np.min(perf_list))

                timestamp = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))
                dir_path = 'logs/fioc_benchmark_%s_%d/%s/' % (problem_str, max_runs, mth)
                file = 'benchmark_%s_%04d_%s.pkl' % (mth, seed, timestamp)
                if not os.path.exists(dir_path):
                    os.makedirs(dir_path)
                with open(os.path.join(dir_path, file), 'wb') as f:
                    save_item = (config_list, perf_list, time_list)
                    pkl.dump(save_item, f)
                print(dir_path, file, 'saved!', flush=True)

