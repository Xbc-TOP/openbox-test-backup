

"""
example cmdline:

python test/optimizer/so/benchmark_so_openbox_math.py --problem branin --n 200 --init 3 --surrogate gp --optimizer scipy --rep 1 --start_id 0

"""
import os
import sys
import time
import pandas as pd
import numpy as np
import argparse
import pickle as pkl
import json

from ConfigSpace import ConfigurationSpace, UniformFloatHyperparameter, UniformIntegerHyperparameter, \
    Constant, CategoricalHyperparameter, InCondition, EqualsCondition, UnParametrizedHyperparameter, \
    ForbiddenEqualsClause, ForbiddenInClause, ForbiddenAndConjunction

sys.path.insert(0, os.getcwd())
from test.reproduction.fioc.fioc_benchmark_function import get_problem
from test.reproduction.test_utils import timeit, seeds
import optuna

parser = argparse.ArgumentParser()
parser.add_argument('--problem', type=str)
parser.add_argument('--n', type=int, default=100)
parser.add_argument('--rep', type=int, default=1)
parser.add_argument('--start_id', type=int, default=0)

args = parser.parse_args()
problem_str = args.problem
max_runs = args.n

rep = args.rep
start_id = args.start_id
mth = 'optuna_nsgaii'

problem = get_problem(problem_str)
config_space = problem.get_configspace(optimizer='smac')
max_runtime_per_trial = 600
task_id = '%s_%s' % (mth, problem_str)


def evaluate(mth, run_i, seed):
    print(mth, run_i, seed, '===== start =====', flush=True)

    def objective_function(trial):
        config = {}
        for i, v in enumerate(config_space.values()):
            name = v.name
            if isinstance(v, UniformFloatHyperparameter):
                config[name] = trial.suggest_float(name, v.lower, v.upper)
            elif isinstance(v, UniformIntegerHyperparameter):
                config[name] = trial.suggest_int(name, v.lower, v.upper)
            elif isinstance(v, CategoricalHyperparameter):
                config[name] = trial.suggest_categorical(name, v.choices)
            elif isinstance(v, Constant):
                config[name] = v.value

        res = problem.evaluate_config(config, optimizer=mth)

        trial.set_user_attr("constraint", tuple(res['constraints']))

        return res['objectives']

    def constraints(trial):
        return trial.user_attrs["constraint"]

    sampler = optuna.samplers.NSGAIISampler(constraints_func=constraints)
    study = optuna.create_study(
        directions=["minimize" for i in range(problem.num_objectives)],
        sampler=sampler,
    )

    study.optimize(objective_function, n_trials=max_runs, timeout=max_runtime_per_trial)

    trials = study.trials

    config_list = []
    perf_list = []
    time_list = []
    global_start_time = time.time()
    for trial in trials:
        config_list.append(list(trial.params.values()))

        perf = trial.values
        perf_list.append(perf)
        time_list.append(trial.datetime_complete.timestamp() - global_start_time)

    config_list = np.array(config_list)
    perf_list = np.array(perf_list)

    return config_list, perf_list, time_list


with timeit('%s all' % (mth,)):
    for run_i in range(start_id, start_id + rep):
        seed = seeds[run_i]
        with timeit('%s %d %d' % (mth, run_i, seed)):
            # Evaluate
            config_list, perf_list, time_list = evaluate(mth, run_i, seed)

            # Save result
            print('=' * 20)
            print(seed, mth, config_list, perf_list, time_list)
            print(seed, mth, 'best perf', np.min(perf_list))

            timestamp = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))
            dir_path = 'logs/fioc_benchmark_%s_%d/%s/' % (problem_str, max_runs, mth)
            file = 'benchmark_%s_%04d_%s.pkl' % (mth, seed, timestamp)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
            with open(os.path.join(dir_path, file), 'wb') as f:
                save_item = (config_list, perf_list, time_list)
                pkl.dump(save_item, f)
            print(dir_path, file, 'saved!', flush=True)


