

"""
example cmdline:

python test/optimizer/so/benchmark_so_openbox_math.py --problem branin --n 200 --init 3 --surrogate gp --optimizer scipy --rep 1 --start_id 0

"""
import os
import sys
import time
import pandas as pd
import numpy as np
import argparse
import pickle as pkl
import json
from hypermapper import optimizer

sys.path.insert(0, os.getcwd())
from test.reproduction.moc.moc_benchmark_function import get_problem
from test.reproduction.test_utils import timeit, seeds
import optuna
from openbox.utils.multi_objective import Hypervolume
from openbox.utils.multi_objective.pareto import is_non_dominated

parser = argparse.ArgumentParser()
parser.add_argument('--problem', type=str)
parser.add_argument('--n', type=int, default=200)
parser.add_argument('--rep', type=int, default=1)
parser.add_argument('--start_id', type=int, default=0)

args = parser.parse_args()
problem_str = args.problem
max_runs = args.n

rep = args.rep
start_id = args.start_id
mth = 'optuna'

problem = get_problem(problem_str)
config_space = problem.get_configspace(optimizer='smac')
max_runtime_per_trial = 600
task_id = '%s_%s' % (mth, problem_str)


def evaluate(mth, run_i, seed):
    print(mth, run_i, seed, '===== start =====', flush=True)

    def objective_function(trial):
        config = {}
        for i, v in enumerate(config_space.values()):
            config["x%d" % (i+1)] = trial.suggest_float("x%d" % (i+1), v.lower, v.upper)

        y = problem.evaluate_config(config, optimizer=mth)

        trial.set_user_attr("constraint", tuple(y['constraints']))

        return tuple(y['objectives'])

    def constraints(trial):
        return trial.user_attrs["constraint"]

    # sampler = optuna.samplers.TPESampler(constraints_func=constraints)
    sampler = optuna.samplers.NSGAIISampler(constraints_func=constraints)
    study = optuna.create_study(
        directions=["minimize" for i in range(problem.num_objectives)],
        sampler=sampler,
    )



    study.optimize(objective_function, n_trials=max_runs, timeout=max_runtime_per_trial)

    trials = study.trials

    config_list = []
    perf_list = []
    cons_list = []
    hv_diffs = []
    time_list = []
    global_start_time = time.time()
    for trial in trials:
        config_list.append(list(trial.params.values()))

        perf = trial.values
        cons = np.array(trial.user_attrs["constraint"])
        cons_list.append(cons)
        if any(cons > 0):
            perf = [9999999.0] * problem.num_objectives
        perf_list.append(perf)
        time_list.append(trial.datetime_complete.timestamp() - global_start_time)
        hv = Hypervolume(problem.ref_point).compute(perf_list)
        hv_diff = problem.max_hv - hv
        hv_diffs.append(hv_diff)

    config_list = np.array(config_list)
    cons_list = np.array(cons_list)
    perf_list = np.array(perf_list)


    is_feas = np.all(cons_list <= 0, axis=1)
    feas_train_obj = perf_list[is_feas]
    pareto_mask = is_non_dominated(feas_train_obj)
    pareto_y = feas_train_obj[pareto_mask]
    pf = pareto_y

    return hv_diffs, pf, config_list, perf_list, time_list


with timeit('%s all' % (mth,)):
    for run_i in range(start_id, start_id + rep):
        seed = seeds[run_i]
        with timeit('%s %d %d' % (mth, run_i, seed)):
            # Evaluate
            hv_diffs, pf, config_list, perf_list, time_list = evaluate(mth, run_i, seed)

            # Save result
            print('=' * 20)
            print(seed, mth, config_list, perf_list, time_list, hv_diffs)
            print(seed, mth, 'best hv_diff:', hv_diffs[-1])
            print(seed, mth, 'max_hv:', problem.max_hv)
            if pf is not None:
                print(seed, mth, 'pareto num:', pf.shape[0])

            timestamp = time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))
            dir_path = 'logs/moc_benchmark_%s_%d/%s_nsgaii/' % (problem_str, max_runs, mth)
            file = 'benchmark_%s_nsgaii_%04d_%s.pkl' % (mth, seed, timestamp)
            if not os.path.exists(dir_path):
                os.makedirs(dir_path)
            with open(os.path.join(dir_path, file), 'wb') as f:
                save_item = (hv_diffs, pf, config_list, perf_list, time_list)
                pkl.dump(save_item, f)
            print(dir_path, file, 'saved!', flush=True)


