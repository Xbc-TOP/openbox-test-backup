import os
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt

# task = 'mo_benchmark_dtlz1-6-5_100'
# task = 'mo_benchmark_zdt2-3_100'
task = 'moc_benchmark_constr_200'
# task = 'moc_benchmark_srn_200'
methods = ['botorch', 'openbox', 'hypermapper', 'optuna', 'optuna_nsgaii']

hv_diffs = {}

for method in methods:
    method_dir = os.path.join('../logs', task, method)
    for file in os.listdir(method_dir):
        with open(os.path.join(method_dir, file), 'rb') as f:
            if method not in hv_diffs:
                hv_diffs[method] = []
            data = np.array(pkl.load(f)[0])[:200]
            if 'constr' in task:
                data += 92.02004226679216
            elif 'srn' in task:
                data += 34229.434882104855
            hv_diffs[method].append(np.log(data))



# Calculate mean and standard deviation for each method
mean_values = {method: np.mean(hv_diffs[method], axis=0) for method in methods}
std_values = {method: np.std(hv_diffs[method], axis=0) for method in methods}

# Create a figure
fig, ax = plt.subplots()

# Plot mean values with standard deviation as shadow
for method in methods:
    ax.plot(mean_values[method], label=method)
    ax.fill_between(range(len(mean_values[method])), mean_values[method] - std_values[method], mean_values[method] + std_values[method], alpha=0.1)

ax.set_title(task)
ax.legend()

# Show the figure
plt.savefig('./images/{}_hv_diffs.png'.format(task))
plt.show()

