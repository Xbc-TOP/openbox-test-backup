
import argparse
import os
import time
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.lines as mlines
import seaborn as sns

def fetch_color_marker(m_list):
    color_dict = dict()
    marker_dict = dict()
    color_list = ['purple', 'royalblue', 'green', 'brown', 'red', 'orange', 'black', 'yellowgreen',
                  'gold', 'cyan', 'violet', 'steelblue']
    markers = ['s', '^', '*', 'v', 'o', '<', 'x', '2', 'p', 'd', '>', '1', '.']

    def fill_values(name, idx):
        color_dict[name] = color_list[idx]
        marker_dict[name] = markers[idx]

    for name in m_list:
        if name.startswith('random'):
            fill_values(name, 3)
        elif name.startswith('openbox'):
            fill_values(name, 4)
        elif name.startswith('botorch'):
            fill_values(name, 1)
        elif name.startswith('hypermapper'):
            fill_values(name, 9)
        elif name.startswith('hyperopt'):
            fill_values(name, 0)
        elif name.startswith('optuna_nsgaii'):
            fill_values(name, 2)
        elif name.startswith('optuna'):
            fill_values(name, 11)
        elif name.startswith('smac'):
            fill_values(name, 5)
        elif name.startswith('ax'):
            fill_values(name, 1)
        else:
            print('color not defined:', name)
            fill_values(name, 0)
    return color_dict, marker_dict


def get_mth_legend(mth, show_mode=True):
    mth_legend = mth
    if show_mode:
        if mth.startswith('random'):
            mth_legend = 'Random'
        elif mth.startswith('openbox'):
            mth_legend = '\\textsc{OpenBox}'
        elif mth.startswith('botorch'):
            mth_legend = 'BoTorch'
        elif mth.startswith('hypermapper'):
            mth_legend = 'HyperMapper'
        elif mth.startswith('hyperopt'):
            mth_legend = 'HyperOpt'
        elif mth.startswith('optuna_nsgaii'):
            mth_legend = 'Optuna'
        elif mth.startswith('optuna'):
            mth_legend = 'Optuna_ori'
        elif mth.startswith('smac'):
            mth_legend = 'SMAC'
        elif mth.startswith('ax'):
            mth_legend = 'Ax'
    # if usetex:
    #     mth_legend = '\\textbf{%s}' % mth_legend
    return mth_legend



def generate_min_array(arr):
    # 初始化结果数组和辅助数组
    result = []
    min_so_far = float('inf')

    # 遍历原始数组
    for num in arr:
        # 更新辅助数组中的最小值
        min_so_far = min(min_so_far, num)
        # 将当前最小值添加到结果数组中
        result.append(min_so_far)

    return result

dir_path = os.path.dirname(os.path.abspath(__file__))

usetex = True
sns.set_style(style='whitegrid')
if usetex:
    plt.rc('text', usetex=True)
# plt.rc('font', **{'size': 16, 'family': 'Helvetica'})

plt.rc('font', size=16.0, family='sans-serif')
plt.rcParams['font.sans-serif'] = "Tahoma"
plt.rcParams['figure.figsize'] = (8.0, 4.5)
if usetex:
    plt.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
plt.rcParams["legend.frameon"] = True
plt.rcParams["legend.facecolor"] = 'white'
plt.rcParams["legend.edgecolor"] = 'gray'
# plt.rcParams["legend.fontsize"] = 16
# label_size = 20
xlabel_size = 24  # todo 26
label_size = 24
tick_size = 16  # todo 20
point_num = 200  # todo
lw = 2
markersize = 8  # todo
markevery = 20
ncol = 2    # number of columns in legend
# plt.switch_backend('agg')
save_fig = True
plt.rcParams["legend.fontsize"] = 16

draw_len = 100

datasets = ['bank32nh', 'cpu_act', 'cpu_small', 'delta_ailerons', 'delta_elevators', 'kin8nm', 'mc1', 'puma32H', 'puma8NH', 'wind']
datasets = ['nasbench201_cifar100']
# datasets = ['nasbench201_ImageNet16-120']
# datasets = ['nasbench201_cifar10']
# datasets = ['cpu_act']

methods = ['smac', 'ax', 'optuna_nsgaii', 'hypermapper', 'openbox']

color_dict, marker_dict = fetch_color_marker(methods)

std_scale = 0.5
std_alpha = 0.15

for dataset in datasets:
    if 'nasbench201' in dataset:
        task = 'fioc_benchmark_%s_100' % dataset
    else:
        task = 'fioc_benchmark_svc_%s_100' % dataset

    handles = list()
    fig, ax = plt.subplots()

    plot_list = []
    legend_list = []

    for mth in methods:
        hv_diffs = []
        method_dir = os.path.join(dir_path, '../logs', task, mth)
        for file in os.listdir(method_dir):
            with open(os.path.join(method_dir, file), 'rb') as f:
                tmp = np.array(pkl.load(f)[1]).reshape(-1)
                if len(tmp) < draw_len:
                    tmp = np.hstack([tmp, np.zeros(draw_len - len(tmp))])
                data = generate_min_array(tmp[:draw_len] + 1)
                hv_diffs.append(data)

        hv_diffs = np.array(hv_diffs)

        if 'svc_cpu_act' in task and mth == 'hypermapper':
            hv_diffs = hv_diffs[[0, 2, 3]]

        x = np.arange(hv_diffs.shape[1]) + 1
        m = np.mean(hv_diffs, axis=0)
        s = np.std(hv_diffs, axis=0)
        # plot
        label_name = get_mth_legend(mth, show_mode=True)
        ax.plot(x, m, lw=lw, label=label_name,
                color=color_dict[mth], marker=marker_dict[mth],
                markersize=markersize, markevery=markevery)
        ax.fill_between(x, m - s * std_scale, m + s * std_scale, alpha=std_alpha, facecolor=color_dict[mth])
        line = mlines.Line2D([], [], color=color_dict[mth], marker=marker_dict[mth],
                             markersize=markersize, label=label_name)
        handles.append(line)

    # ax.set_title(task)
    ax.set_xlim(0, draw_len)
    if 'svc_bank32nh' in task:
        ax.set_ylim(0.21, 0.22)
    elif 'svc_cpu_act' in task:
        ax.set_ylim(0.08, 0.20)
    elif 'svc_cpu_small' in task:
        ax.set_ylim(0.10, 0.20)
    elif 'svc_delta_ailerons' in task:
        ax.set_ylim(0.058, 0.065)
    elif 'svc_delta_elevators' in task:
        ax.set_ylim(0.11, 0.12)
    elif 'kin8nm' in task:
        ax.set_ylim(0.265, 0.277)
        plt.yticks([0.265, 0.27, 0.275], ['0.265', '0.270', '0.275'])
    elif 'mc1' in task:
        ax.set_ylim(0.20, 0.50)
    elif 'puma32H' in task:
        ax.set_ylim(0.35, 0.37)
    elif 'puma8NH' in task:
        ax.set_ylim(0.177, 0.18)
    elif 'wind' in task:
        ax.set_ylim(0.136, 0.145)
    elif 'nasbench201_cifar10_' in task:
        ax.set_ylim(0.05, 0.10)
    elif 'nasbench201_cifar100_' in task:
        ax.set_ylim(0.265, 0.32)
    elif 'nasbench201_ImageNet16-120' in task:
        ax.set_ylim(0.52, 0.625)

    legend = ax.legend(handles=handles, loc=1, ncol=ncol, bbox_to_anchor=(1.015, 1.028))

    if 'nasbench201_cifar100' in task:
        plt.subplots_adjust(top=0.978, right=0.975, left=0.11, bottom=0.14)
    elif 'svc' in task:
        plt.subplots_adjust(top=0.980, right=0.975, left=0.11, bottom=0.15)

    frame = legend.get_frame()
    frame.set_alpha(0.5)

    xlabel = 'Trials'
    ylabel = 'Error Rate'

    xlabel = '\\textbf{%s}' % xlabel
    ylabel = '\\textbf{%s}' % ylabel
    ax.set_xlabel(xlabel, fontsize=xlabel_size)
    ax.set_ylabel(ylabel, fontsize=label_size)

    plt.tick_params(labelsize=tick_size)
    # plt.tight_layout()
    if save_fig:
        plt.savefig(os.path.join(dir_path, './images/{}_fioc_diffs.png'.format(task)))
        plt.savefig(os.path.join(dir_path, './images/{}_fioc_diffs.pdf'.format(task)))
        plt.show()
    else:
        plt.show()




