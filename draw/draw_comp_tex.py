"""
example cmdline:

python test/benchmark_plot_new.py --dataset covtype --R 27 --mths hyperband-n8,amfesv20-n8

"""
import argparse
import os
import time
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.lines as mlines
import seaborn as sns

usetex = True
di = 10

task = 'moc_benchmark_constr_200'
# task = 'moc_benchmark_srn_200'
mths = ['hypermapper', 'botorch', 'optuna_nsgaii', 'openbox']

sns.set_style(style='whitegrid')

if usetex:
    plt.rc('text', usetex=True)
# plt.rc('font', **{'size': 16, 'family': 'Helvetica'})

plt.rc('font', size=16.0, family='sans-serif')
plt.rcParams['font.sans-serif'] = "Tahoma"

plt.rcParams['figure.figsize'] = (8.0, 4.5)
if usetex:
    plt.rcParams['text.latex.preamble'] = r"\usepackage{amsmath}"
plt.rcParams["legend.frameon"] = True
plt.rcParams["legend.facecolor"] = 'white'
plt.rcParams["legend.edgecolor"] = 'gray'
# plt.rcParams["legend.fontsize"] = 16
# label_size = 20
xlabel_size = 24  # todo 26
label_size = 24
tick_size = 16  # todo 20

point_num = 200  # todo
lw = 2
markersize = 8  # todo
markevery = 20

ncol = 2    # number of columns in legend

# plt.switch_backend('agg')

R = 27
save_fig = True
plt.rcParams["legend.fontsize"] = 16

def fetch_color_marker(m_list):
    color_dict = dict()
    marker_dict = dict()
    color_list = ['purple', 'royalblue', 'green', 'brown', 'red', 'orange', 'black', 'yellowgreen',
                  'gold', 'cyan', 'violet', 'steelblue']
    markers = ['s', '^', '*', 'v', 'o', '<', 'x', '2', 'p', 'd', '>', '1', '.']

    def fill_values(name, idx):
        color_dict[name] = color_list[idx]
        marker_dict[name] = markers[idx]

    for name in m_list:
        if name.startswith('random'):
            fill_values(name, 3)
        elif name.startswith('openbox'):
            fill_values(name, 4)
        elif name.startswith('botorch'):
            fill_values(name, 1)
        elif name.startswith('hypermapper'):
            fill_values(name, 9)
        elif name.startswith('optuna'):
            fill_values(name, 2)
        else:
            print('color not defined:', name)
            fill_values(name, 0)
    return color_dict, marker_dict


def get_mth_legend(mth, show_mode=True):
    mth_legend = mth
    if show_mode:
        if mth.startswith('random'):
            mth_legend = 'Random'
        elif mth.startswith('openbox'):
            mth_legend = '\\textsc{OpenBox}'
        elif mth.startswith('botorch'):
            mth_legend = 'BoTorch'
        elif mth.startswith('hypermapper'):
            mth_legend = 'HyperMapper'
        elif mth.startswith('optuna'):
            mth_legend = 'Optuna'
    # if usetex:
    #     mth_legend = '\\textbf{%s}' % mth_legend
    return mth_legend


handles = list()
fig, ax = plt.subplots()

color_dict, marker_dict = fetch_color_marker(mths)

std_scale = 0.5
std_alpha = 0.15

plot_list = []
legend_list = []
for mth in mths:
    hv_diffs = []
    dir_path = '../logs/%s/%s/' % (task, mth)
    for file in os.listdir(dir_path):
        with open(os.path.join(dir_path, file), 'rb') as f:
            datas = pkl.load(f)
        hv_diff = np.array(datas[0])[:point_num]
        if 'constr' in task:
            hv_diff += 92.02004226679216
        elif 'srn' in task:
            hv_diff += 34229.434882104855

        hv_diffs.append(np.log(hv_diff) / np.log(di))

    if 'srn' in task:
        # hv_diffs = np.array([hv_diffs[0], hv_diffs[1], hv_diffs[3], hv_diffs[4]])
        hv_diffs = np.array(hv_diffs)
    else:
        hv_diffs = np.array(hv_diffs)


    x = np.arange(hv_diffs.shape[1]) + 1
    m = np.mean(hv_diffs, axis=0)
    s = np.std(hv_diffs, axis=0)
    # plot
    label_name = get_mth_legend(mth, show_mode=True)
    ax.plot(x, m, lw=lw, label=label_name,
            color=color_dict[mth], marker=marker_dict[mth],
            markersize=markersize, markevery=markevery)
    ax.fill_between(x, m - s * std_scale, m + s * std_scale, alpha=std_alpha, facecolor=color_dict[mth])
    line = mlines.Line2D([], [], color=color_dict[mth], marker=marker_dict[mth],
                         markersize=markersize, label=label_name)
    handles.append(line)

ax.set_xlim(0, 200)
if 'constr' in task:
    # if di == 10:
    #     ax.set_ylim(-0.1, 1.2)
    if di == 10:
        ax.set_ylim(-0.0, 1.1)
    elif di == np.e:
        ax.set_ylim(-0.1, 3.0)
if 'srn' in task:
    if di == 10:
        ax.set_ylim(3.1, 4.45)

legend = ax.legend(handles=handles, loc=1, ncol=ncol, bbox_to_anchor=(1.015, 1.028))
if 'constr' in task:
    plt.subplots_adjust(top=0.978, right=0.975, left=0.098, bottom=0.14)
else:
    plt.subplots_adjust(top=0.99, right=0.975, left=0.098, bottom=0.14)
frame = legend.get_frame()
frame.set_alpha(0.5)

xlabel = 'Trials'
ylabel = 'Hv Difference (log)'

xlabel = '\\textbf{%s}' % xlabel
ylabel = '\\textbf{%s}' % ylabel
ax.set_xlabel(xlabel, fontsize=xlabel_size)
ax.set_ylabel(ylabel, fontsize=label_size)

plt.tick_params(labelsize=tick_size)
# plt.tight_layout()
if save_fig:
    plt.savefig('./images/%s_hv_%d.pdf' % (task, di))
    plt.savefig('./images/%s_hv_%d.png' % (task, di))
else:
    plt.show()
