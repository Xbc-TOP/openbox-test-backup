"""
example cmdline:

python test/benchmark_plot_new.py --dataset covtype --R 27 --mths hyperband-n8,amfesv20-n8

"""
import argparse
import os
import time
import numpy as np
import pickle as pkl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.lines as mlines
import seaborn as sns

from utils import setup_exp, descending, create_plot_points

usetex = True

sns.set_style(style='whitegrid')

if usetex:
    plt.rc('text', usetex=True)
# plt.rc('font', **{'size': 16, 'family': 'Helvetica'})

plt.rc('font', size=16.0, family='sans-serif')
plt.rcParams['font.sans-serif'] = "Tahoma"

plt.rcParams['figure.figsize'] = (8.0, 4.5)
if usetex:
    plt.rcParams['text.latex.preamble'] = [r"\usepackage{amsmath}"]
plt.rcParams["legend.frameon"] = True
plt.rcParams["legend.facecolor"] = 'white'
plt.rcParams["legend.edgecolor"] = 'gray'
# plt.rcParams["legend.fontsize"] = 16
# label_size = 20
xlabel_size = 26  # todo 24
tick_size = 20  # todo 14

point_num = 300  # todo
lw = 2
markersize = 8  # todo
markevery = 20

ncol = 2    # number of columns in legend

# plt.switch_backend('agg')

default_mths = 'hyperband-n8,bohb-n8,mfes-n8,amfesv20-n8'

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str)
parser.add_argument('--mths', type=str, default=default_mths)
parser.add_argument('--R', type=int, default=27)
parser.add_argument('--runtime_limit', type=int)    # if you don't want to use default setup
parser.add_argument('--model', type=str, default='xgb')
parser.add_argument('--default_value', type=float, default=0.0)
parser.add_argument('--save', type=int, default=0)
parser.add_argument('--legend', type=float, default=16)

args = parser.parse_args()
dataset = args.dataset
mths = args.mths.split(',')
R = args.R
model = args.model
default_value = args.default_value
save_fig = bool(args.save)
plt.rcParams["legend.fontsize"] = args.legend if args.legend > 0 else 16

if model == 'lstm':
    label_size = 24  # todo 20
else:
    label_size = 26  # todo 24


def fetch_color_marker(m_list):
    color_dict = dict()
    marker_dict = dict()
    color_list = ['purple', 'royalblue', 'green', 'brown', 'red', 'orange', 'black', 'yellowgreen',
                  'gold', 'dimgray', 'violet', 'steelblue']
    markers = ['s', '^', '*', 'v', 'o', '<', 'x', '2', 'p', 'd', '>', '1', '.']

    def fill_values(name, idx):
        color_dict[name] = color_list[idx]
        marker_dict[name] = markers[idx]

    for name in m_list:
        if name.startswith('random'):
            fill_values(name, 3)
        elif name.startswith('bohb'):
            fill_values(name, 2)
        elif name.startswith('bo'):
            fill_values(name, 10)
        elif name.startswith('sh'):
            fill_values(name, 9)
        else:
            print('color not defined:', name)
            fill_values(name, 0)
    return color_dict, marker_dict


def get_mth_legend(mth, show_mode=True):
    mth_legend = mth
    if show_mode:
        if mth.startswith('random'):
            mth_legend = 'Random'
        elif mth.startswith('bohb'):
            mth_legend = 'BOHB'
        elif mth.startswith('bo'):
            mth_legend = 'BO'
        elif mth.startswith('sh'):
            mth_legend = 'SHA'
    if usetex:
        mth_legend = '\\textbf{%s}' % mth_legend
    return mth_legend


def plot_setup(_model, _dataset):
    y_range = None
    if _dataset == 'covtype':
        y_range = [-0.940, -0.880]
        plt.subplots_adjust(top=0.975, right=0.960, left=0.095, bottom=0.18)

    if y_range is not None:
        y_range[0] += y_bias.get(_model, 0)
        y_range[1] += y_bias.get(_model, 0)
        if model in ['xgb', 'resnet']:
            y_range[0] *= 100
            y_range[1] *= 100
        ax.set_ylim(*y_range)


y_bias = dict(
    xgb=1,
    nasbench201=100,
    resnet=1,
)

print('start', dataset)
handles = list()
fig, ax = plt.subplots()

# setup
_, runtime_limit, _ = setup_exp(dataset, 1, 1, 1)
if args.runtime_limit is not None:
    runtime_limit = args.runtime_limit
plot_setup(model, dataset)
color_dict, marker_dict = fetch_color_marker(mths)

std_scale = 0.5
std_alpha = 0.15

plot_list = []
legend_list = []
result = dict()
for mth in mths:
    stats = []
    dir_path = 'data/benchmark_%s/%s-%d/%s/' % (model, dataset, runtime_limit, mth)
    for file in os.listdir(dir_path):
        if file.startswith('new_record_%s-%s-' % (mth, dataset)) and file.endswith('.pkl'):
            with open(os.path.join(dir_path, file), 'rb') as f:
                raw_recorder = pkl.load(f)
            recorder = []
            for record in raw_recorder:
                # if record.get('n_iteration') is not None and record['n_iteration'] < R:
                #     print('error abandon record by n_iteration:', R, mth, record)
                #     continue
                if record['global_time'] > runtime_limit:
                    print('abandon record by runtime_limit:', runtime_limit, mth, record)
                    continue
                recorder.append(record)
            recorder.sort(key=lambda rec: rec['global_time'])
            # print([(rec['global_time'], rec['return_info']['loss']) for rec in recorder])
            print('new recorder len:', mth, len(recorder), len(raw_recorder))
            timestamp = [rec['global_time'] for rec in recorder]
            perf = descending([rec['return_info']['loss'] for rec in recorder])
            stats.append((timestamp, perf))

    x, m, s = create_plot_points(stats, 0, runtime_limit, point_num=point_num, default=default_value)
    m = m + y_bias.get(model, 0)
    result[mth] = (x, m, s)
    if model in ['xgb', 'resnet']:
        m = m * 100  # Caution: do not m *= 100
        s = s * 100
    # plot
    label_name = get_mth_legend(mth, show_mode=True)
    ax.plot(x, m, lw=lw, label=label_name,
            color=color_dict[mth], marker=marker_dict[mth],
            markersize=markersize, markevery=markevery)
    ax.fill_between(x, m - s * std_scale, m + s * std_scale, alpha=std_alpha, facecolor=color_dict[mth])
    line = mlines.Line2D([], [], color=color_dict[mth], marker=marker_dict[mth],
                         markersize=markersize, label=label_name)
    handles.append(line)

ax.set_xlim(1, runtime_limit)
ax.xaxis.set_major_locator(ticker.MultipleLocator(runtime_limit // 8))
if args.legend > 0:
    legend = ax.legend(handles=handles, loc=1, ncol=ncol, bbox_to_anchor=(1.015, 1.028))
    frame = legend.get_frame()
    frame.set_alpha(0.5)
    #frame.set_facecolor('none')

xlabel = 'Wall Clock Time (s)'
if model == 'lstm':
    ylabel = 'Average Valid Perplexity'
elif model == 'math':
    ylabel = 'Average Objective Value'
# elif model == 'nasbench201':
#     if usetex:
#         ylabel = 'Average Validation Error (\\%)'
#     else:
#         ylabel = 'Average Validation Error (%)'
else:
    ylabel = 'Average Valid Error'
if usetex:
    xlabel = '\\textbf{%s}' % xlabel
    ylabel = '\\textbf{%s}' % ylabel
ax.set_xlabel(xlabel, fontsize=xlabel_size)
ax.set_ylabel(ylabel, fontsize=label_size)

plt.tick_params(labelsize=tick_size)
# plt.tight_layout()
if save_fig:
    plt.savefig('%s_%s.pdf' % (model, dataset))
else:
    plt.show()
